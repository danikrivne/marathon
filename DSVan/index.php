<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <main>
        <?php
        $pdo = new PDO("pgsql:host=127.0.0.1;dbname=crud", 'marathon', 'marathon2020');
        $stmt = $pdo->prepare('SELECT * FROM article');
        $stmt->execute();
        $articles = $stmt->fetchAll();
        foreach ($articles as $article):?>
            <div class="main__news__block">
                <div class="main__news__block-row">
                    Назва: <?=$article['name']?>
                </div>
                <div class="main__news__block-row">
                    Створений: <?=$article['created_at']?>
                </div>
                <div class="main__news__block-row">
                    <a href="update.php?id=<?=$article['id']?>" title="update">update</a>
                </div>
                <div class="main__news__block-row">
                    <a href="delete.php?id=<?=$article['id']?>" title="delete">delete</a>
                </div>
            </div>
        <?endforeach?>

        <a href="create.php">create</a>
    </main>
</body>
</html>